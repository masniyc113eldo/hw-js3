let userInput = prompt("Введіть число:");


if (isNaN(userInput)) {
  console.log("Введено некоректне число!");
} else {
  let number = parseInt(userInput);
  let foundMultiples = false;

  for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) {
      console.log(i);
      foundMultiples = true;
    }
  }

  if (!foundMultiples) {
    console.log("Sorry, no numbers");
  }
}
